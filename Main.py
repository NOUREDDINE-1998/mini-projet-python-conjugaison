import sys
from Past import past
from Present import present
from Futur import futur

def menu():
    print("\n\t\t1. Voulez-vous conjuguer le verbe au Futur ? ")
    print("\t\t2. Voulez-vous conjuguer le verbe au Présent ? ")
    print("\t\t3. Voulez-vous conjuguer le verbe au Passé composé ? \n")
    try:
        temps=int(input("==> Choisir le temps :   "))
        match temps:
            case 1:
                futur(verbe)
            case 2:
                present(verbe)
            case 3:
                past(verbe)
            case default:
                print("Choix invalide.")
                menu()
    except ValueError: 
        print("Vous avez saisi une chaîne de caractère et non un nombre.")
                
def check():
    
    check_verb=verbe.endswith("er")
    if verbe.isdigit():
        print("Ooops!, That's not a string!")
    elif (check_verb!=True or verbe=="aller"):
        print(verbe.capitalize()+" n'est pas un verbe du 1er groupe.")
    else :
        menu()
    
while True:    
    verbe=input("\n==> Saisir un verbe du 1er groupe: ").lower()
    if verbe=="":
        print("\nFin du programme.\n")
        sys.exit()
    else:
        check()
    
        
