• Proposition  : Conjugaison d’un verbe 
Implémentez un programme qui lit au clavier un verbe du premier groupe (se terminant
par « er ») puis affiche la conjugaison de ce verbe au temps choisi par l’utilisateur (Futur,
Présent ou Passé composé).
Le programme doit demander à l’utilisateur au début de saisir le verbe à conjuguer (qui
doit être du 1er groupe). Le programme affiche ensuite sur l’écran :
1. Voulez-vous conjuguer le verbe au Futur ?
2. Voulez-vous conjuguer le verbe au Présent ?
3. Voulez-vous conjuguer le verbe au Passé composé ?
L’utilisateur choisi le numéro qui présente le temps de conjugaison voulu puis le
programme doit afficher sur l’écran la conjugaison du verbe selon le choix de l’utilisateur.
