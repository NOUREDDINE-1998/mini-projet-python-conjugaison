def past(verbe):
	verb=verbe
	auxetre=["arriver","entrer","monter","décéder","rester","passer","tomber","rentrer","rester","retourner"]
	avoir={
				"J'":"ai",
				"Tu":"as",
				"Il/Elle/On":"a",
				"Nous":"avons",
				"Vous":"avez",
				"Ils/Elles":"ont",
			}
	etre={
				"Je":"suis",
				"Tu":"es",
				"Il/Elle/On":"est",
				"Nous":"sommes",
				"Vous":"êtes",
				"Ils/Elles":"sont",
			}								
	dictpreflechi={
				"Je me":"suis",
				"Tu t'":"es",
				"Il/Elle/On s'":"est",
				"Nous nous":"sommes",
				"Vous vous ":"êtes",
				"Ils/Elles se":"sont",
			}
	if(verb in auxetre):
		for pp,aux in etre.items():
			if (pp.startswith("Nous") or pp.startswith("Vous") or  pp.startswith("Ils")):
				print("\t\t"+pp+" "+aux+" "+verb[:-2]+"é(es)")
			else:
				print("\t\t"+pp+" "+aux+" "+verb[:-2]+"é(e)")
					
	elif (verb.startswith("se ")):
		for pp,term in dictpreflechi.items():
			if (pp.startswith("Nous") or pp.startswith("Vous") or  pp.startswith("Ils")):
				print("\t\t"+pp+" "+term+" "+verb[3:-2]+"é(es)")
			else:
				print("\t\t"+pp+" "+term+" "+verb[3:-2]+"é(e)")
	else:
		for pp,aux in avoir.items():
			print("\t\t"+pp+' '+aux+' '+verb[:-2]+"é")