
dlt=["amonceler", "appeler", "atteler", "bateler", "bosseler", "botteler", "bourreler", "bretteler", "canneler", "carreler", "chanceler", "cordeler", "craqueler", "désensorceler", "dételer", "ensorceler", "étiqueter", "interpeler", "jeter", "jumeler", "morceler", "museler", "niveler", "projeter", "rejeter", "rappeler", "renouveler", "ressemeler", "ruisseler", "voleter"]
dacht=["acheter","bégueter","corseter","crocheter","fileter","fureter","haleter","racheter","geler","celer","ciseler","congeler","décongeler","déceler","dégeler","démanteler","désurgeler","écarleter","harceler","marteler","modeler","peler","receler","recongeler","regeler","remodeler","surgeler"]
def present(verbe):
	verb=verbe
	double=dlt
	dictpreflechi={
				"Je me":"e",
				"Tu te":"es",
				"Il/Elle/On se":"e",
				"Nous nous":"ons",
				"Vous vous ":"ez",
				"Ils/Elles se":"ent",
			}
	dict={
				"Je":"e",
				"Tu":"es",
				"Il/Elle/On":"e",
				"Nous":"ons",
				"Vous":"ez",
				"Ils/Elles":"ent",
			}
	if (verb.startswith("se ")):
		for pp,term in dictpreflechi.items():
			print("\t\t"+pp+" "+verb[3:-2]+term)
	elif (verb in double):
		for pp,term in dict.items():
			if (verb[-3:-2]=='l'):
				if (verb.startswith("a") or verb.startswith("e") or verb.startswith("é") or verb.startswith("i") or verb.startswith("u") ):
					if (pp=="Nous" or pp=="Vous"):
						print("\t\t"+pp+" "+verb[:-2]+term)
					else:
						print("\t\t"+pp.replace("Je","J'")+" "+verb[:-2]+"l"+term)
				else:
					print("\t\t"+pp+" "+verb[:-2]+"l"+term)
			elif (verb[-3:-2]=='t'):
				if (pp=="Nous" or pp=="Vous"):
					print("\t\t"+pp+" "+verb[:-2]+term)
				else:
					print("\t\t"+pp+" "+verb[:-2]+"t"+term) 
	elif(verb in dacht):
		for pp,term in dict.items():
			if(verb[-3:-2]=="t"):
				if (verb.startswith("a") or verb.startswith("e") or verb.startswith("é") or verb.startswith("i") or verb.startswith("u")):
					if (pp=="Nous" or pp=="Vous"):
						print("\t\t"+pp+" "+verb[:-2]+term)
					else:
						print("\t\t"+pp.replace("Je","J'")+" "+verb[:-4]+"èt"+term)
			elif(verb[-3:-2]=="l"):
				if (verb.startswith("a") or verb.startswith("e") or verb.startswith("é") or verb.startswith("i") or verb.startswith("u")):
					if (pp=="Nous" or pp=="Vous"):
						print("\t\t"+pp+" "+verb[:-2]+term)
					else:
							print("\t\t"+pp.replace("Je","J'")+" "+verb[:-4]+"èl"+term)
				elif (pp=="Nous" or pp=="Vous"):
					print("\t\t"+pp+" "+verb[:-2]+term)
				else:
					print("\t\t"+pp.replace("Je","J'")+" "+verb[:-4]+"èl"+term)
	else:
		for pp,term in dict.items():
			if (verb[-3:-2]=='g' and pp=="Nous"):
				print("\t\t"+pp+" "+verb[:-2]+"e"+term)
			elif (verb.startswith("a") and pp=="Je"):
					print("\t\t"+pp.replace("Je","J'")+verb[:-2]+term)
			elif (verb[-3:-2]=='c' and pp=="Nous"):
				print("\t\t"+pp+" "+verb[:-3]+"ç"+term)
			else:
				print("\t\t"+pp+" "+verb[:-2]+term)
		