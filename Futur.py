
dlt=["amonceler", "appeler", "atteler", "bateler", "bosseler", "botteler", "bourreler", "bretteler", "canneler", "carreler", "chanceler", "cordeler", "craqueler", "désensorceler", "dételer", "ensorceler", "étiqueter", "interpeler", "jeter", "jumeler", "morceler", "museler", "niveler", "projeter", "rejeter", "rappeler", "renouveler", "ressemeler", "ruisseler", "voleter"]
dacht=["acheter","bégueter","corseter","crocheter","fileter","fureter","haleter","racheter","geler","celer","ciseler","congeler","décongeler","déceler","dégeler","démanteler","désurgeler","écarleter","harceler","marteler","modeler","peler","receler","recongeler","regeler","remodeler","surgeler"]
def futur(verbe):
    verb=verbe
    double=dlt
    dictpreflechi={
                "Je me":"ai",
                "Tu te":"as",
                "Il/Elle/On se":"a",
                "Nous nous":"ons",
                "Vous vous ":"ez",
                "Ils/Elles se":"ont",
            }
    pronom_personnel = ['Je', 'Tu', 'Il/Elle', 'Nous', 'Vous', 'Ils/Elles']
    terminaison = ['ai', 'as', 'a', 'ons', 'ez', 'ont']
    dict1 = dict(zip(pronom_personnel, terminaison))
    if (verb.startswith("se ")):
        for pp,term in dictpreflechi.items():
            print("\t\t"+pp+" "+verb[3:]+term)
    elif (verb in double):
        for pp,term in dict1.items():
            if (verb[-3:-2]=='l'):
                if (verb.startswith("a") or verb.startswith("e") or verb.startswith("é") or verb.startswith("i") or verb.startswith("u") ):
                    print("\t\t"+pp.replace("Je","J'")+" "+verb[:-2]+"ler"+term)
                else:
                    print("\t\t"+pp+" "+verb[:-2]+"ler"+term)
            elif (verb[-3:-2]=='t'):
                print("\t\t"+pp+" "+verb[:-2]+"ter"+term)
    elif(verb in dacht):
        for pp,term in dict1.items():
            if(verb[-3:-2]=="t"):
                if (verb.startswith("a") or verb.startswith("e") or verb.startswith("é") or verb.startswith("i") or verb.startswith("u")):
                    print("\t\t"+pp.replace("Je","J'")+" "+verb[:-4]+"èter"+term)
                else:
                    print("\t\t"+pp+" "+verb[:-4]+"èter"+term)
            elif(verb[-3:-2]=="l"):
                if (verb.startswith("a") or verb.startswith("e") or verb.startswith("é") or verb.startswith("i") or verb.startswith("u")):
                    print("\t\t"+pp.replace("Je","J'")+" "+verb[:-4]+"èler"+term)
                else:
                    print("\t\t"+pp+" "+verb[:-4]+"èler"+term)
                        
    else:
                for pp,term in dict1.items():
                    if (verb.startswith("a") and pp=="Je"):
                        print("\t\t"+pp.replace("Je","J'")+verb+term) 
                    else:
                        print("\t\t"+pp+" "+verb+term)   
                    